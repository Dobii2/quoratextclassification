from sklearn.feature_extraction.text import TfidfVectorizer


def build_features(X=None, fit=False) -> None:
    tfidf_vectorizer = TfidfVectorizer()

    if fit:
        tfidf_vectorizer.fit(X)

    X = tfidf_vectorizer.transform(X)
    return X
