import pandas as pd
import argparse


def visualize_balance_classes(input_path: str, output_path=".") -> None:
    df = pd.read_csv(input_path)
    counts = df['target'].value_counts()
    counts.plot(kind='bar')
    counts.savefig(f"{output_path}/class_balance.png")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_path', type=str, help='Input path for dataset')
    parser.add_argument('output_path', type=str, help='Output path for save picture')
    args = parser.parse_args()
    visualize(args.input_path, args.output_path)
