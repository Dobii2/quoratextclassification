import seaborn as sns
from sklearn.metrics import confusion_matrix


def visualize_confusion_matrix(y_true, y_pred):
    confution_matr = confusion_matrix(y_true, y_pred)
    ax = sns.heatmap(confution_matr, linewidths=0.01, annot=True, fmt='.1f', color='red')
    ax.set_title('Seaborn Confusion Matrix with labels\n\n')
    ax.set_xlabel('Predictions')
    ax.set_ylabel('True Values')

    ax.xaxis.set_ticklabels(['False', 'True'])
    ax.yaxis.set_ticklabels(['False', 'True'])
