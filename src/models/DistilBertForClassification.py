import torch
from transformers import DistilBertModel


class DistilBertForClassification(torch.nn.Module):
  def __init__(self, config: dict):
    super(DistilBertForClassification, self).__init__()
    self.model_name = config['model_name']
    self.n_classes = config['num_classes']
    self.dropout_rate = config['dropout_rate']
    self.bert = DistilBertModel.from_pretrained(config['model_name'])
    self.pre_classifier = torch.nn.Sequential(
        torch.nn.Linear(768, 768),
        torch.nn.ReLU(),
        torch.nn.Dropout(self.dropout_rate),
        torch.nn.Linear(768, self.n_classes)
        )

  def forward(self, input_ids, attention_mask):
    output = self.bert(
        input_ids=input_ids,
        attention_mask=attention_mask
    )

    hidden_state = output[0]
    hidden_state = hidden_state[:, 0]
    output = self.pre_classifier(hidden_state)
    return output