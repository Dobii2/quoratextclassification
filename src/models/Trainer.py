import torch
import numpy as np
from tqdm import tqdm
from src.models import DistilBertForClassification


class Trainer:
    def __init__(self, config: dict) -> None:
        self.config = config
        self.n_epochs = self.config['n_epochs']
        self.batch_size = self.config['batch_size']
        self.model = None
        self.optimizer = None
        self.opt_fn = lambda model: torch.optim.Adam(model.parameters(), self.config['lr'])
        self.history = None
        self.loss_fn = torch.nn.CrossEntropyLoss()
        self.device = self.config['device']

    def fit(self, model, train_dataloader, val_dataloader):
        self.model = model.to(self.device)
        self.optimizer = self.opt_fn(self.model)
        self.history = {
            'train_loss': [],
            'valid_loss': [],
            'val_acc': []
        }

        for epoch in range(self.n_epochs):
            print(f'Epoch {epoch + 1}/{self.n_epochs}')
            train_info = self.train_epoch(train_dataloader)
            val_info = self.val_epoch(val_dataloader)
            self.history['train_loss'].extend(train_info['loss'])
            self.history['valid_loss'].extend(val_info['loss'])
            self.history['val_acc'].extend(val_info['acc'])

        return self.model.eval()

    def train_epoch(self, train_dataloader) -> dict:
        losses = []
        self.model.train()
        train_dataloader = tqdm(train_dataloader)
        for batch in train_dataloader:
            ids = batch['ids'].to(self.device, dtype=torch.long)
            mask = batch['mask'].to(self.device, dtype=torch.long)
            targets = batch['targets'].to(self.device, dtype=torch.long)

            outputs = self.model(ids, mask)
            loss = self.loss_fn(outputs, targets)

            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()
            loss_val = loss.item()
            train_dataloader.set_description(f"Loss={loss_val:.3}")
            losses.append(loss_val)
        return {'loss': losses}

    def val_epoch(self, val_dataloader) -> dict:
        self.model.eval()
        all_logits = []
        all_labels = []
        with torch.no_grad():
            val_dataloader = tqdm(val_dataloader)
            for batch in val_dataloader:
                ids = batch['ids'].to(self.device, dtype=torch.long)
                mask = batch['mask'].to(self.device, dtype=torch.long)
                targets = batch['targets'].to(self.device, dtype=torch.long)
                outputs = self.model(ids, mask)
                all_logits.append(outputs)
                all_labels.append(targets)

            all_logits = torch.cat(all_logits).to(self.device)
            all_labels = torch.cat(all_labels).to(self.device)
            loss = self.loss_fn(all_logits, all_labels).item()
            acc = (all_logits.argmax(1) == all_labels).float().mean().item()
            val_dataloader.set_description(f'Loss={loss:.3}; Acc={acc:.3}')

        return {
            'loss': loss,
            'acc': acc
        }

    def predict(self, test_dataloader) -> np.array:
        if not self.model:
            raise RuntimeError("You should train the model first")
        self.model.eval()
        predictions = []
        with torch.no_grad():
            for batch in test_dataloader:
                ids = batch['ids'].to(self.device, dtype=torch.long)
                mask = batch['mask'].to(self.device, dtype=torch.long)
                outputs = self.model(ids, mask)
                predictions.extend(outputs.argmax(1).tolist())
        return np.asarray(predictions)

    def save(self, path: str) -> None:
        if not self.model:
            raise RuntimeError("You should train the model first")

        checkpoint = {
            "config": self.model.config,
            "trainer_config": self.config,
            "model_name": self.model.model_name,
            "model_state_dict": self.model.state_dict()
        }

        torch.save(checkpoint, path)

    @classmethod
    def load(cls, path: str):
        ckpt = torch.load(path)
        keys = ["config", "trainer_config", "model_state_dict"]
        for key in keys:
            if key not in ckpt:
                raise RuntimeError(f"Missing key {key} in checkpoint")
        new_model = DistilBertForClassification(
            ckpt['model_name'],
            ckpt["config"]
        )
        new_model.load_state_dict(ckpt["model_state_dict"])
        new_trainer = cls(ckpt["trainer_config"])
        new_trainer.model = new_model
        new_trainer.model.to(new_trainer.device)
        return new_trainer
