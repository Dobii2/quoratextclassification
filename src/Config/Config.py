import torch


class Config:
    GENERAL_PATH = "."
    DATA_PATH = f"{GENERAL_PATH}/data"
    TRAIN_PATH = f"{DATA_PATH}/raw_data/train.csv"
    TEST_PATH = f"{DATA_PATH}/raw_data/test.csv"
    SUBMIT = f"{DATA_PATH}/raw_data/sample_submission.csv"
    SAVE_PATH = f"{DATA_PATH}/models"
    EMBEDDINGS_WIKI_NEWS = f"{DATA_PATH}/embeddings/wiki-news-300d-1M/wiki-news-300d-1M.vec"
    EMBEDDINGS_GOOGLE_NEWS = f"{DATA_PATH}/embeddings//GoogleNews-vectors-negative300/GoogleNews-vectors-negative300.bin"
    EMBEDDINGS_GLOVE = f"{DATA_PATH}/embeddings/glove.840B.300d/glove.840B.300d.txt"
    EMBEDDINGS_PARAGRAM = f"{DATA_PATH}/embeddings/paragram_300_sl999/paragram_300_sl999.txt"
    RANDOM_STATE = 42
    SEED = 42
    VALID_SIZE = 0.2
    MODEL_CONFIG = {
        'model_name': 'distilbert-base-uncased',
        'dropout_rate': 0.2,
        'num_classes': 2,
    }

    TRAIN_CONFIG = {
        'pad_len' : 256,
        'batch_size': 32,
        'lr': 3e-5,
        'n_epochs': 5,
        'max_len': 128,
        'weight_decay': 1e-6,
        'device': 'cuda:0' if torch.cuda.is_available() else 'cpu'
    }

