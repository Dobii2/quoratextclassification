import pandas as pd
import argparse
from ydata_profiling import ProfileReport


def eda(input_path: str, output_path: str) -> None:
    df = pd.read_csv(input_path)
    profile_train = ProfileReport(df, title="Report DataFrame")
    profile_train.to_file(f"{output_path}/report_dataframe.html")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('input_path', type=str, help="Input path for dataset")
    parser.add_argument('output_path', type=str, help="Output path for save report")
    args = parser.parse_args()
    eda(parser.input_path, parser.output_path)

