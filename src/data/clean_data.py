import re
import unicodedata
import contractions
import pandas as pd

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import SnowballStemmer
from nltk.stem.wordnet import WordNetLemmatizer

import argparse


def clean_text(text):
    # HTML-теги: первый шаг - удалить из входного текста все HTML-теги
    clean_text = re.sub("<[^<]+?>", "", text)

    # URL и ссылки: далее - удаляем из текста все URL и ссылки
    clean_text = re.sub(r"http\S+", "", clean_text)

    # Эмоджи и эмотиконы: используем собственную функцию для преобразования эмоджи в текст
    # Важно понимать эмоциональную окраску обрабатываемого текста

    clean_text = clean_text.lower()

    # Убираем все пробелы
    # Так как все данные теперь представлены словами - удалим пробелы
    clean_text = re.sub("\s+", " ", clean_text)

    # Преобразование символов с диакритическими знаками к ASCII-символам
    clean_text = (
        unicodedata.normalize("NFKD", clean_text)
        .encode("ascii", "ignore")
        .decode("utf-8", "ignore")
    )

    # Разворачиваем сокращения: текст часто содержит конструкции вроде "don't" или "won't",
    clean_text = contractions.fix(clean_text)

    # Убираем специальные символы: избавляемся от всего, что не является "словами"
    clean_text = re.sub("[^a-zA-Z0-9\s]", "", clean_text)

    # Стоп-слова: удаление стоп-слов - это стандартная практика очистки текстов

    stop_words = set(stopwords.words("english"))
    tokens = word_tokenize(clean_text)
    tokens = [token for token in tokens if token not in stop_words]
    clean_text = " ".join(tokens)

    # Знаки препинания: далее - удаляем из текста все знаки препинания
    clean_text = re.sub(r"[^\w\s]", "", clean_text)

    return clean_text


def lemmatization(text):
    lemma = WordNetLemmatizer()
    return [lemma.lemmatize(word=w, pos="v") for w in text]


def stemming(text):
    stemmer = SnowballStemmer("english")
    text = " ".join(stemmer.stem(word) for word in text.split(" "))
    return text


def preprocessing(input_path: str, output_path: str):
    df = pd.read_csv(input_path)
    df["question_text"] = df["question_text"].apply(clean_text)
    df["question_text"] = df["question_text"].apply(stemming)
    df.to_csv(output_path, index=False)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_path", type=str, help="Input path for dataset")
    parser.add_argument("output_path", type=str, help="Output path for dataset")
    args = parser.parse_args()
    preprocessing(args.input_path, args.output_path)
