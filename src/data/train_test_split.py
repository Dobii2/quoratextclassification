def train_test_split(data, train_frac=0.85):
    """
    Splits the data into train and test parts, stratifying by labels.
    Should it shuffle the data before split?
    :param data: dataset to split
    :param train_frac: proportion of train examples
    :return: texts and labels for each split
    """
    n_toxicity_ratings = 6
    train_labels = []
    val_labels = []
    train_texts = []
    val_texts = []
    for label in range(n_toxicity_ratings):
        texts = data[label]
        n_train = int(len(texts) * train_frac)
        n_val = len(texts) - n_train
        train_texts.extend(texts[:n_train])
        val_texts.extend(texts[n_train:])
        train_labels += [label] * n_train
        val_labels += [label] * n_val
    return train_texts, train_labels, val_texts, val_labels